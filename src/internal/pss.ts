import { Dealer } from 'zeromq';

/**
 * Clase PSS (Peer Sample Service)
 * A partir de un conjunto de conexiones correctas
 * devuelve una muestra aleatoria sin repetición
 * de K conexiones a procesos correctos
 */
export default class PSS {

    /**
     * Permuta aleatoriamente el conjunto o lista de conexiones
     * @param peers conjunto de conexiones
     */
    // private static shuffle(peers: Dealer[]): Dealer[] {
    //     var ctr = peers.length;
    //     var temp: Dealer;
    //     var index: number;
    //     var indexes: number[] = [];
    
    //     while(ctr > 0) {
    //         index = Math.floor(Math.random() * ctr);
    //         ctr--;
    //         indexes.push(index);

    //         temp = peers[ctr];
    //         peers[ctr] = peers[index];
    //         peers[index] = temp;
    //     }

    //     return peers;
    // }

    /**
     * Permuta aleatoriamente el conjunto o lista de conexiones
     * @param peers conjunto de conexiones
     */
    private static shuffle(peers: Dealer[]): Dealer[] {
        for(let i = peers.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [peers[i], peers[j]] = [peers[j], peers[i]];
        }
        return peers;
    }

    /**
     * Devuelve una muestra aleatoria de K conexiones a procesos correctos
     * @param peers conjunto de conexiones
     * @param K tamaño de la muestra
     */
    public static sample(peers: Dealer[], K: number): Dealer[] {

        // Si no hay conexiones suficientes para la muestra, se devuelven todos
        if(peers.length < K) {
            return peers;
        }

        peers = PSS.shuffle(peers);
        return peers.slice(0, K);
    }
}
