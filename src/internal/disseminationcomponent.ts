import Process from './process';
import Event from './event';
import Clock from './clock';
import { LogicalClock } from './clock';
import { Dealer } from "zeromq";
import PSS from './pss';
import Ball from './ball';

/**
 * Clase DisseminationComponent
 * Componente que se encarga de la disusión de los
 * eventos a traves de la red entre procesos
 */
export default class DisseminationComponent {
    
    // Variables algoritmo EpTO
    private K: number;                              // Tamaño de la muestra aleatorio de procesos
    public TTL: number;                             // Maximo numero de saltos de los eventos
    private static deltha: number = 200;            // Round duration in milliseconds
    private _nextBall: { [id: string]: Event; };    // Conjunto de eventos a enviar en la proxima ronda
    private _peers: Dealer[];                       // Conjunto de conexiones correctas
    
    // Variables adicionales
    private _process: Process;                      // Proceso al que pertenece
    private _nextRoundInterval: NodeJS.Timeout;     // Variable que guarda el interval repetitivo de las rondas
    private _logical: boolean;                      // Si el reloj es logico
    private _logicalClock: LogicalClock;            // Reloj logico

    /**
     * Constructor del componente
     * @param process proceso al que pertenece
     */
    constructor(process: Process, n: number, f: number, logical: boolean) {
        this._process = process;
        this._nextBall = {};
        this._peers = [];
        this.TTL = n;
        this.K = n - f;
        this._logical = logical;
        this._logicalClock = new LogicalClock();
    }

    /**
     * Devuelve el conjunto de conexiones correctas a otros procesos
     */
    get peers(): Dealer[] {
        return this._peers;
    }

    /**
     * Prepara el envio del evento
     * @param event evento a enviar
     */
    public epToBroadcast(event: Event): void {
        if(this._logical) {
            event.ts = this._logicalClock.getTime();
        } else {
            event.ts = Clock.getTime();
        }
        event.ttl = 0;
        event.sourceId = this._process.id;
        this._nextBall[event.id] = event;
    }

    /**
     * Recibe y procesa una ball de otro proceso
     * @param ball ball recibida
     */
    public recieveBall(ball: Ball): void {
        
        // Para cada evento del ball
        ball.events.forEach((event: Event) => {
            // Si no ha llegado al límite de saltos
            if(event.ttl < this.TTL) {
                
                const localEvent: Event = this._nextBall[event.id];
                // Si ya existe el evento aquí
                if(localEvent) {
                    // Actualizamos su ttl y es preciso
                    if(localEvent.ttl < event.ttl) {
                        localEvent.ttl = event.ttl;
                    }
                } else {
                    // Si no, lo añadimos a la lista de proximos eventos a difundir
                    this._nextBall[event.id] = event;
                }
            }

            // Update clock
            if(this._logical) {
                this._logicalClock.updateClock(event.ts);
            }
        });
    }

    /**
     * Ejecuta la siguiente ronda
     * @param context contexto del componente (Necesario para poder acceder a sus variables
     * desde el conexto de la funcion (Ahora es la del interval))
     */
    public nextRound(context: DisseminationComponent): void {
        // Aumenta los ttls de todos los eventos a difundir
        Object.keys(context._nextBall).forEach((id: string) => {
            context._nextBall[id].ttl++;
        });

        // Obtiene al lista de los eventos
        const events: Event[] = Object.values(context._nextBall);

        // Si no es nula, escoge una muestra aleatoria de conexiones,
        // crea una ball con los eventos, la serializa y la envia a todas las conexiones
        if(events.length > 0) {
            const selectedPeers: Dealer[] = PSS.sample(context._peers, this.K);

            const ball = new Ball(events);
            selectedPeers.forEach((peer: Dealer) => {
                peer.send(ball.serialize());
            });
        }

        // Finalmente envia al componente de ordenacion los eventos y limpia la lista para la siguiente ronda
        context._process.orderingComponent.orderEvents(events);
        context._nextBall = {};
    }

    // Funciones adicionales

    /**
     * Comienza las rondas
     */
    public startFirstRound(): void {
        this._nextRoundInterval = setInterval(this.nextRound, DisseminationComponent.deltha, this);
    }

    /**
     * Detiene las rondas
     */
    public endRounds(): void {
        clearInterval(this._nextRoundInterval);
    }
}
